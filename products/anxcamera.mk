#
# Copyright (C) 2020 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# ANX
PRODUCT_COPY_FILES += \
    vendor/anxcamera/configs/ANXCamera/anx.js:$(TARGET_COPY_OUT_SYSTEM)/etc/ANXCamera/anx.js

# Permissions
PRODUCT_COPY_FILES += \
    vendor/anxcamera/configs/default-permissions/anxcamera-permissions.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/default-permissions/anxcamera-permissions.xml \
    vendor/anxcamera/configs/permissions/privapp-permissions-anxcamera.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-anxcamera.xml

# Shim
PRODUCT_PACKAGES += \
    libmediaplayer_shim

# Sysconfig
PRODUCT_COPY_FILES += \
    vendor/anxcamera/configs/sysconfig/anxcamera-hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/anxcamera-hiddenapi-package-whitelist.xml

# Props
PRODUCT_PRODUCT_PROPERTIES += \
	ro.hardware.camera=xiaomi

$(call inherit-product, vendor/anxcamera/common/common-vendor.mk)
